﻿<#
.SYNOPSIS
  Синхронизация локальных репозиториев с сервером GitLab

.DESCRIPTION
  Скрипт синхронизирует локальные репозиторий с удаленными репозиториеми сервера GitLab. 
  При необходимости и возможности перемещает заголовоки веток. Клонирует репозиторий при отсутвии.

.PARAMETER GitLabDomain
  Адрес сервера Gitlab (без http)
  
.PARAMETER GitLabToken
  Приватный ключ сервера Gitlab
  
.PARAMETER StartFolder
  Путь к папке с локальными репозиториями
  
.PARAMETER Group
  Группа на сервере GitLab. Если отсутвует будут синхронизированы все доступные
  
.PARAMETER FastForward
  Необходимо перемещят заголовок ветки или нет

.OUTPUTS
  Скрипт выводит результаты синхронизации на экран

.EXAMPLE
  $GitLabDomain = 'git-example.com'
  $GitLabToken = git config --get "Gitlab.`"$GitLabDomain`".TOKEN" #Add to config first

  $Labels = 'bug,Bugs=My-Bug;feature,Feature=My-Feature;confirmed='
		
  #Path to script
  & $(Join-Path $PSScriptRoot BatchPullGroup.ps1.ps1) $GitLabDomain $GitLabToken 'C:\MyProjects'
#>
Param (
    [Parameter (Mandatory = $true)]
    [string]$GitLabDomain,
    [Parameter (Mandatory = $true)]
    [string]$GitLabToken,
    [Parameter (Mandatory = $true)]
    [string]$StartFolder,
    [string]$Group = '',
    [bool]$FastForward = $true
)

$GitLabAPI = "http://$GitLabDomain/api/v4"

function Import-Group ($Group) {

    for ($i = 1; $i -ge 0; $i++) {

        $Project = Invoke-RestMethod -Uri "$GitLabAPI/groups/$Group/projects?order_by=name&sort=asc&per_page=1&page=$i" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

        if ($Project.Count -eq 0) { break }

        $ProjectPath = Join-Path $StartFolder $Project.path_with_namespace
        $ProjectSSH = $Project.ssh_url_to_repo
        $ProjectBranch = $Project.default_branch
       
        & $(Join-Path $PSScriptRoot BatchPullDirect.ps1) $ProjectPath $ProjectSSH $ProjectBranch
    }

    return $i - 1
}

if ($Group) {
    Import-Group $Group
}
else {
    for ($g = 1; $g -ge 0; $g++) {

        $GitLabGroup = Invoke-RestMethod -Uri "$GitLabAPI/groups?per_page=1&page=$g" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

        if ($GitLabGroup.Count -eq 0) { break }

        Write-Host "$($GitLabGroup.name) Group"

        [void](Import-Group($GitLabGroup.id))
    }
}

