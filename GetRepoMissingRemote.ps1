﻿<#
.SYNOPSIS
  Определяет доступность удаленных репозиториев

.DESCRIPTION
  Скрипт находит все репозитории в указанной папке, после чего проверяет доступность хотя бы одного удаленного репозитория

.PARAMETER StartFolder
  Папка с проектами

.OUTPUTS
  Пути к репозиториям, в которых все ссылки на удаленные репозитории недействительны

.EXAMPLE
  GetRepoMissingRemote.ps1 'C:\MyProjects'
#>

Param (
    [Parameter (Mandatory = $true)]
    [string]$StartFolder
)

function Test-Remote ($RepoPath) {

    & { $temp = git --git-dir="$RepoPath" rev-parse head } 2>$null

    if (-Not ($LASTEXITCODE -eq 0)) { return $false }

    ForEach ($Name in (git --git-dir="$RepoPath" remote)) {
        & { $temp = git --git-dir="$RepoPath" ls-remote --heads $Name } 2>$null
        
        if ($LASTEXITCODE -eq 0) { return $true }
    }
    
    return $false
} 

$Projects = Get-ChildItem -Recurse -Directory -Path $StartFolder | Where-Object { (Test-Path (Join-Path $_.FullName '.git') -PathType Container) } | `
    Where-Object { !(Test-Remote (Join-Path $_.FullName '.git')) }

Write-Output $Projects | Select-Object -ExpandProperty FullName