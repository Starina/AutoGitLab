﻿<#
.SYNOPSIS
  Фиксирует скопированные папки и отпралвяет в удаленные репозиторий

.DESCRIPTION
  Репозиторий, в который необходимо скопировать папки должен содержать файл настройки `CopyDirs.json`. 
  В удаленном репозитории необходимо сделать хотя бы одну фиксацию. Скрипт работает с текущими настройками 
  репозитория, он не измененяет название удаленного репозитория и имя текущей ветки. Не забывайте в файле настроек
  экранировать необходимые символы (например: \ должен стать \\).

  В настоящее время поддерживается метод копирования только ROBOCOPY. 

.PARAMETER Repository
  Путь к папке с репозиторием

.PARAMETR LogOut
  Путь к папке для логов

.EXAMPLE
  Пример файла настройки
```json
[
   {
      "Name":"First",
	  "Method":"robocopy",
      "Source":"c:\\Temp",
      "Dest":"Temp",
      "Include":[
         "*.*"
      ],
      "ExcludeFiles":[
      ],
      "ExcludeDirs":[
      ]
   }
]
```
#>
Param (
    [Parameter (Mandatory = $true)]
    [string]$Repository,
    [string]$LogOut = ''
)

chcp 65001

function JoinWithQuote($Append) {
    If (-Not ($Append.Count -eq 0)) {
        return ('"{0}"' -f ($Append -join '" "'))
    } 
    Else {
        return ''
    }
}

$ConfigFileName = 'CopyDirs.json'

Set-Location $Repository
if (-Not (Test-Path ".\$ConfigFileName")) { Wtite-Error 'No configuration file' -ErrorAcrion Stop }

#region Log
$RepositoryName = (Get-Item '.').BaseName
$ScriptName = $MyInvocation.MyCommand.Name -replace '.ps1'

If ($LogOut) {
    [void](New-Item -ItemType Directory -Force $LogOut)
    $LogFileName = Join-Path $LogOut "$ScriptName.$RepositoryName-$(((Get-Date).ToUniversalTime()).ToString("yyyyMMddThhmmss")).log"
}
#endregion

#region Copy files
$Sources = Get-Content ".\$ConfigFileName" | Out-String | ConvertFrom-Json

ForEach ($Source in $Sources) {
    $Dest = New-Item -ItemType Directory -Force (Join-Path $Repository $Source.Dest)

    $Include = JoinWithQuote $Source.Include

    $cmdArgs = @("`"$($Source.Source)`"", "`"$Dest`"", $Include, '/MIR')

    $ExcludeFiles = JoinWithQuote $Source.ExcludeFiles    
    $ExcludeDirs = JoinWithQuote $Source.ExcludeDirs 

    if ($ExcludeFiles) { $cmdArgs += $(@("/XF", "$ExcludeFiles")) }
    if ($ExcludeDirs) { $cmdArgs += $(@("/XD", "$ExcludeDirs")) }
    if ($LogOut) { $cmdArgs += "/unilog:`"$LogFileName`"" }
      
    [void](robocopy @cmdArgs) #при установке лога возвращает его имя  
}
#endregion

#region Git commands
$RepRef = git symbolic-ref HEAD --quiet
$OriginName = git for-each-ref --format='%(upstream:remotename)' $RepRef

git fetch $OriginName --quiet

$RemoteBranch = git for-each-ref --format='%(upstream:short)' 'refs/heads/master'

git reset --mixed $RemoteBranch --quiet

if ($(git status --short)) {
    git add --all
    git commit --message="Auto copy" --quiet
    git push $OriginName --quiet
}
#endregion