﻿<#
.SYNOPSIS
  Замена меток замечаний в проекте GitLab

.DESCRIPTION
  Скрипт заменяет метки замечаний в конкретном проекте на сервере GitLab

.PARAMETER GitLabDomain
  Адрес сервера Gitlab (без http)
  
.PARAMETER GitLabToken
  Приватный ключ сервера Gitlab
  
.PARAMETER ProjectId
  Идентификатор проекта на сервер Gitlab
  
.PARAMETER Labels
  Перечень меток. Строка разделяемая точкой с запятой, в которой список старых меток приравнивается к одной новой

.OUTPUTS
  Скрипт выводит результаты замены на экран

.EXAMPLE
  See ReplaceIssueLabelInGroup.ps1
#>
Param (
    [Parameter (Mandatory = $true)]
    [string]$GitLabDomain,
    [Parameter (Mandatory = $true)]
    [string]$GitLabToken,
    [Parameter (Mandatory = $true)]
    [int]$ProjectId,
    [Parameter (Mandatory = $true)]
    [string]$Labels
)

$GitLabAPI = "http://$GitLabDomain/api/v4"

$LabelTable = $Labels.Replace(";", "`r`n") | ConvertFrom-StringData #https://stackoverflow.com/questions/27631834

function Read-ProjectIssueLabel($GitLabAPI, $GitLabToken, $ProjectId) {
    $LabelList = @()

    for ($i = 1; $i -ge 0; $i++) {
        
        $Label = Invoke-RestMethod -Uri "$GitLabAPI/projects/$ProjectId/labels?per_page=1&page=$i" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

        if ($Label.Count -eq 0) { break } 

        $LabelList += $($Label.Name)
    }

    return $LabelList
}

function ReplaceProjectIssueLabel($GitLabAPI, $GitLabToken, $ProjectId, $LabelTable) {

    $ProjectLabelList = Read-ProjectIssueLabel $GitLabAPI $GitLabToken $ProjectId

    if ($ProjectLabelList.Count -eq 0) { return }

    foreach ($LabelKey in $LabelTable.Keys) {
    
        foreach ($OldLabel in $($LabelKey -split ',')) {

            if (-not ($ProjectLabelList -ccontains $OldLabel)) { continue }

            if ($LabelTable[$LabelKey]) {             
                for ($i = 1; $i -ge 0; $i++) {

                    $Issue = Invoke-RestMethod -Uri "$GitLabAPI/projects/$ProjectId/issues?labels=$OldLabel&per_page=1&page=$i" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

                    if ($Issue.Count -eq 0) { break } 
                    
                    if ($Issue.labels -contains $LabelTable[$LabelKey]) { continue }
                    
                    Write-Host "`t Issue $($Issue.iid) replace ""$OldLabel"" to ""$($LabelTable[$LabelKey])"""

                    $IssueLabels = @($Issue.labels)

                    $IssueLabels += $LabelTable[$LabelKey]

                    $NewIssueLabel = $IssueLabels -Join ","

                    [void](Invoke-RestMethod -Method Put -Uri "$GitLabAPI/projects/$ProjectId/issues/$($Issue.iid)?labels=$NewIssueLabel" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" })
                }
            }

            [void](Invoke-RestMethod -Method DELETE -Uri "$GitLabAPI/projects/$ProjectId/labels?name=$OldLabel" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" })

            Write-Host "`t`t delete label ""$OldLabel"""                
        }
    }
}

ReplaceProjectIssueLabel $GitLabAPI $GitLabToken $ProjectId $LabelTable