﻿<#
.SYNOPSIS
  Замена меток замечаний во всех проектах группы GitLab

.DESCRIPTION
  Скрипт заменяет метки замечаний во всех проектах на сервере GitLab

.PARAMETER GitLabDomain
  Адрес сервера Gitlab (без http)
  
.PARAMETER GitLabToken
  Приватный ключ сервера Gitlab
  
.PARAMETER GroupName
  Имя группы на сервер Gitlab
  
.PARAMETER Labels
  Перечень меток. Строка разделяемая точкой с запятой, в которой список старых меток приравнивается к одной новой

.OUTPUTS
  Скрипт выводит результаты замены на экран

.EXAMPLE
  $GitLabDomain = 'git-example.com'
  $GitLabToken = git config --get "Gitlab.`"$GitLabDomain`".TOKEN" #Add to config first

  $Labels = 'bug,Bugs=My-Bug;feature,Feature=My-Feature;confirmed='
		
  #Path to script
  & $(Join-Path $PSScriptRoot ReplaceIssueLabelInGroup.ps1) $GitLabDomain $GitLabToken 'MyGroup' $Labels
#>

Param (
    #[Parameter (Mandatory = $true)]
    [string]$GitLabDomain,
    #[Parameter (Mandatory = $true)]
    [string]$GitLabToken,
    #[Parameter (Mandatory = $true)]
    [string]$GroupName,
    #[Parameter (Mandatory = $true)]
    [string]$Labels
)

$GitLabAPI = "http://$GitLabDomain/api/v4"

for ($p = 1; $p -ge 0; $p++) {

    $Project = Invoke-RestMethod -Uri "$GitLabAPI/groups/$GroupName/projects?per_page=1&page=$p" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

    if ($Project.Count -eq 0) { break }

    Write-Host "Project ""$($Project.Name)"""

    & $(Join-Path $PSScriptRoot ReplaceProjectIssueLabel.ps1) $GitLabDomain $GitLabToken $($Project.id) $Labels    
}