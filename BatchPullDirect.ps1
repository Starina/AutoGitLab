﻿<#
.SYNOPSIS
  Синхронизация конкретного репозитория с сервером GitLab

.DESCRIPTION
  Скрипт синхронизирует локальный репозиторий с удаленным репозиторием сервера GitLab. При возможности перемещается заголовок ветки

.PARAMETER GitLabDomain
  Адрес сервера Gitlab (без http)
  
.PARAMETER GitLabToken
  Приватный ключ сервера Gitlab
  
.PARAMETER ProjectPath
  Путь к локальному репозиторию 
  
.PARAMETER ProjectSSH
  SSH удлаенного репозитория
  
.PARAMETER ProjectBranch
  Ветка по умолчанию

.OUTPUTS
  Скрипт выводит результаты замены на экран

.EXAMPLE
  See BatchPullGroup.ps1, BatchPullProject.ps1
#>
Param (
    [Parameter (Mandatory = $true)]
    [string]$ProjectPath,
    [Parameter (Mandatory = $true)]
    [string]$ProjectSSH,
    [Parameter (Mandatory = $true)]
    [string]$ProjectBranch,
    [bool]$FastForward = $true
)
function Get-LocalDefaultBranch($RemoteName, $BranchName) {
    ForEach ($Branch in (git for-each-ref --format='%(refname:short)=%(upstream:short)' refs/heads/ | ConvertFrom-StringData)) {
        if ("$RemoteName/$BranchName" -eq $Branch.Values) { return $Branch.Keys }
    }
}

function Get-RemoteName($SSH) {
    ForEach ($Name in (git remote)) {
        if ((git remote get-url $Name) -eq $SSH) { return $Name }
    }
}
   
$ProjectDir = New-Item -ItemType Directory -Force $ProjectPath

Set-Location $ProjectDir.FullName

if (-Not (Test-Path (Join-Path $ProjectDir.FullName '.git')  -PathType Container)) {
    Write-Host "$($ProjectDir.BaseName) Clone" -ForegroundColor White
    git clone $ProjectSSH $ProjectDir.FullName --quiet            
}
else {
    #Отсутсвует удаленные ссылки
    if (-Not (git ls-remote $ProjectSSH)) {
        Write-Host "$($ProjectDir.BaseName) Refs not exists" -ForegroundColor Red
        continue
    }

    $RemoteName = Get-RemoteName $ProjectSSH 

    Write-Host "$($ProjectDir.BaseName) Fetch" -ForegroundColor Gray
    git fetch $RemoteName --quiet

    if (-Not($FastForward)) { continue }

    #Отсутсвует ветка по умолчанию
    if (-Not ($ProjectBranch)) {
        Write-Host "$($ProjectDir.BaseName) No default branch" -ForegroundColor Red
        continue
    }

    #Есть изменения в репозиториях
    if (git status --short) {
        Write-Host "$($ProjectDir.BaseName) Repo not clean" -ForegroundColor Red
        continue
    }

    #Текущий коммит не запушен
    if (-Not (git branch -r --contains $(git rev-parse HEAD))) {
        Write-Host "$($ProjectDir.BaseName) Repo not push" -ForegroundColor Red
        continue
    }

    $LocalDefaultBranch = Get-LocalDefaultBranch -RemoteName $RemoteName -BranchName $ProjectBranch
    if (-Not ($LocalDefaultBranch)) { 
        git checkout -t "$RemoteName/$ProjectBranch" --quiet
        Write-Host "$($ProjectDir.BaseName) Checkout" -ForegroundColor Blue
    }
    else {
        if (-Not (git branch -r --contains $(git log --pretty=format:'%H' -n 1 $LocalDefaultBranch))) {
            Write-Host "$($ProjectDir.BaseName) $LocalDefaultBranch branch not push" -ForegroundColor Red
            continue
        }  
                
        git checkout $LocalDefaultBranch --quiet
        $RepoHEAD = git rev-parse HEAD
        git merge --ff-only "$RemoteName/$ProjectBranch" --quiet
        if ($RepoHEAD -ne $(git rev-parse HEAD)) { Write-Host "$($ProjectDir.BaseName) Merge" -ForegroundColor Magenta }
    }            
}