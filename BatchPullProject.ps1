﻿<#
.SYNOPSIS
  Синхронизация локальных репозиториев с сервером GitLab

.DESCRIPTION
  Скрипт синхронизирует локальные репозиторий с удаленным репозиторием сервера GitLab. 
  При возможности перемещает заголовоки веток. При отсутвии репозитоория он клонируется.

.PARAMETER GitLabDomain
  Адрес сервера Gitlab (без http)
  
.PARAMETER GitLabToken
  Приватный ключ сервера Gitlab
  
.PARAMETER ProjectPath
  Путь к локальному репозиторию 
  
.PARAMETER Group
  Группа на сервере GitLab. Если отсутвует будут синхронизированы все доступные
  
.PARAMETER FastForward
  Необходимо перемещят заголовок ветки или нет

.OUTPUTS
  Скрипт выводит результаты синхронизации на экран

.EXAMPLE
  $GitLabDomain = 'git-example.com'
  $GitLabToken = git config --get "Gitlab.`"$GitLabDomain`".TOKEN" #Add to config first

  $Labels = 'bug,Bugs=My-Bug;feature,Feature=My-Feature;confirmed='
		
  #Path to script
  & $(Join-Path $PSScriptRoot BatchPullGroup.ps1.ps1) $GitLabDomain $GitLabToken 'C:\MyProjects'
#>
Param (
    [Parameter (Mandatory = $true)]
    [string]$GitLabDomain,
    [Parameter (Mandatory = $true)]
    [string]$GitLabToken,
    [Parameter (Mandatory = $true)]
    [string]$StartFolder,
    [Parameter (Mandatory = $true)]
    [string]$ProjectId,
    [bool]$FastForward = $true
)

$GitLabAPI = "http://$GitLabDomain/api/v4"

$Project = Invoke-RestMethod -Uri "$GitLabAPI/projects/$ProjectId" -Headers @{'PRIVATE-TOKEN' = "$GitLabToken" }

if ($Project.Count -eq 0) { break }

$ProjectPath = Join-Path $StartFolder $Project.path_with_namespace
$ProjectSSH = $Project.ssh_url_to_repo
$ProjectBranch = $Project.default_branch
       
& $(Join-Path $PSScriptRoot BatchPullDirect.ps1) $ProjectPath $ProjectSSH $ProjectBranch